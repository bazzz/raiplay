package main

import (
	"flag"
	"log"

	"gitlab.com/bazzz/raiplay"
)

func main() {
	url := ""
	flag.StringVar(&url, "url", "", "The raiplay URL pointing to the page holding the episodes.")
	flag.Parse()
	if url == "" {
		log.Fatal("url cannot be empty")
	}
	raiplay.Get(url)
}
