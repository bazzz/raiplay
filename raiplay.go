package raiplay

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/bazzz/downloader"
)

const outputPath = "./output"

const baseURL = "https://www.raiplay.it"

// Get downloads the url content, writes nfos, thumbs and a list of downloadlinks.
func Get(url string) {
	if !strings.HasPrefix(url, baseURL) {
		log.Fatal("url must start with " + baseURL)
		return
	}
	err := os.MkdirAll(outputPath, os.ModePerm)
	if err != nil {
		log.Fatal(err)
		return
	}
	show, err := getShow(url)
	if err != nil {
		log.Fatal(err)
		return
	}
	downloadLinks := make([]string, 0)
	episodes := getEpisodes(show)
	prevEpisodenr := 0
	for i, episode := range episodes {
		downloadLinks = append(downloadLinks, baseURL+episode.Link)
		seasonnr, err := strconv.Atoi(episode.SeasonNr)
		if err != nil {
			if len(episode.SeasonNr) == 0 {
				seasonnr = 1
			} else {
				seasonlength := 4 // Some seasons are like '2019/20', therefore just take the first 4 chars.
				seasonnr, err = strconv.Atoi(episode.SeasonNr[:seasonlength])
				if err != nil {
					log.Panicln("cannot convert", episode.SeasonNr, "to int:", err)
				}
			}
		}
		episodenr, err := strconv.Atoi(episode.EpisodeNr)
		if err != nil {
			// Puntate do not have a season number and are sorted the other way around.
			episodenr = len(episodes) - i
		}
		if episodenr == prevEpisodenr {
			log.Println("Found episode", episodenr, "which is the same as previous episode. Changing it to", episodenr+1)
			episodenr++
		}
		prevEpisodenr = episodenr
		nfo := NFO{
			Title:     episode.Title,
			Plot:      episode.Description,
			EpisodeNr: episodenr,
			SeasonNr:  seasonnr,
			Language:  "it",
		}
		name := getName(show.Name, nfo)
		if err := nfo.Save(outputPath, name); err != nil {
			log.Println("cannot write nfo:", outputPath, name, err)
		}
		img := episode.Images["landscape"]
		if img != "" {
			imgURL := baseURL + img
			imgPath := filepath.Join(outputPath, name+"-thumb")
			if err = downloader.GetLazyHTTP(imgURL, imgPath); err != nil {
				log.Println("cannot download image:", imgURL, imgPath, err)
			}
		}
	}
	downloadLinksFile := filepath.Join(outputPath, "links.txt")
	downloadLinksData := []byte(strings.Join(downloadLinks, "\n"))
	err = os.WriteFile(downloadLinksFile, downloadLinksData, os.ModePerm)
	if err != nil {
		log.Fatal(err)
		return
	}
}

func getShow(url string) (*Show, error) {
	data, err := httpGet(url)
	if err != nil {
		return nil, err
	}
	block, set, path, err := getEpisodesInfo(string(data))
	if err != nil {
		return nil, err
	}
	for strings.Count(url, "/") > 4 {
		url = url[:strings.LastIndex(url, "/")]
	}
	url += "/" + block + "/" + set + "/" + path
	data, err = httpGet(url)
	if err != nil {
		return nil, err
	}

	show := Show{}
	if err = json.Unmarshal(data, &show); err != nil {
		return nil, err
	}
	show.Name = strings.Trim(show.Name, " \t\n") // Sometimes there is needless whitespace in the name.
	return &show, nil
}

func getEpisodes(show *Show) []Episode {
	result := make([]Episode, 0)
	for _, collection := range show.Collections {
		for _, season := range collection.Seasons {
			result = append(result, season.Episodes...)
		}
	}
	return result
}

func httpGet(url string) (bytes []byte, err error) {
	response, err := http.Get(url)
	if err != nil {
		return
	}
	defer response.Body.Close()
	bytes, err = io.ReadAll(response.Body)
	return
}

func getEpisodesInfo(data string) (block, set, path string, err error) {
	pos := strings.Index(data, "<rai-episodes")
	if pos < 0 {
		err = errors.New("could not find '<rai-episodes'")
		return
	}
	data = data[pos:]
	pos = strings.Index(data, "</rai-episodes>")
	if pos < 0 {
		err = errors.New("could not find '</rai-episodes>'")
		return
	}
	data = data[:pos]

	block, err = getAttr(data, "block")
	if err != nil {
		return
	}
	set, err = getAttr(data, "set")
	if err != nil {
		return
	}
	path, err = getAttr(data, "episode_path")
	if err != nil {
		return
	}
	return
}

func getAttr(data string, attr string) (string, error) {
	attr += "=\""
	pos := strings.Index(data, attr)
	if pos < 0 {
		return "", errors.New("could not find '" + attr + "'")
	}
	data = data[pos+len(attr):]
	pos = strings.Index(data, "\"")
	if pos < 0 {
		return "", errors.New("could not find end of '" + attr + "'")
	}
	return data[:pos], nil
}

func getName(showName string, nfo NFO) string {
	result := showName
	season := strconv.Itoa(nfo.SeasonNr)
	if len(season) == 4 && strings.HasPrefix(season, "20") {
		// changes year 20xx into season xx
		season = season[2:]
	}
	result += " " + season + "x" + strconv.Itoa(nfo.EpisodeNr) + " " + nfo.Title
	return clean(result)
}

func clean(name string) string {
	regex := regexp.MustCompile(`[\\:/?<>|]+`)
	result := regex.ReplaceAllLiteralString(name, "")
	return result
}
