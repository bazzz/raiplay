package raiplay

// Show is een show.
type Show struct {
	Name        string       `json:"name"`
	ID          string       `json:"id"`
	Collections []Collection `json:"seasons"`
}

// Collection houd een collectie van seizoenen vast.
type Collection struct {
	Seasons []Season `json:"episodes"`
}

// Season is een seizoen
type Season struct {
	Episodes []Episode `json:"cards"`
}

// Episode is een episode of een puntata.
type Episode struct {
	Link        string            `json:"weblink"`
	SeasonNr    string            `json:"season"`
	EpisodeNr   string            `json:"episode"`
	Title       string            `json:"episode_title"`
	Description string            `json:"description"`
	Images      map[string]string `json:"images"`
}
