package raiplay

import (
	"encoding/xml"
	"os"
	"path/filepath"
)

// NFO is a holder for nfo xml data.
type NFO struct {
	XMLName   xml.Name `xml:"episodedetails"`
	Title     string   `xml:"title"`
	Plot      string   `xml:"plot"`
	SeasonNr  int      `xml:"season"`
	EpisodeNr int      `xml:"episode"`
	Language  string   `xml:"language"`
}

// Save writes the nfo content to the file named 'name'.nfo at the provided path. No extention must be provided for name, .nfo will be added. If the file already exists it does nothing.
func (n NFO) Save(path string, name string) error {
	path = filepath.Join(path, name+".nfo")
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		return nil // file exists
	}
	xmlText, err := xml.MarshalIndent(n, " ", "  ")
	if err != nil {
		return err
	}
	xmlText = []byte(xml.Header + string(xmlText))
	err = os.WriteFile(path, xmlText, os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}
